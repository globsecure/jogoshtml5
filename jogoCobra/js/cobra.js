/**
 * Obter objeto que serve de campo para o jogo
 *
 * @type {*}
 */
var campoDeJogo = $("#campoDeJogo")[0];

var contexto2D = campoDeJogo.getContext("2d");
var largura = $("#campoDeJogo").width();
var altura = $("#campoDeJogo").height();

/**
 * Permite salvar a largura da célula em uma variável para o controle fácil
 * @type {number}
 */
var tamanhoCorpoCobra   = 30;//10;
var tamanhoCorpoRato    = 30;//10;
var direcao;
var ratoPosicao;
var pontos;

/**
 * Corpo da cobra
 * @type {Array}
 */
var cobraCorpo = new Array();


// Elemento ready é criado para executar as funções
// apenas depois que a página está carregada
$(document).ready(function(){
    /**
     * Função aguarda uma tecla presionada no documento ou seja na tela.
     */
    $(document).keydown(function(e){
        var key = e.which;
        /**
         * Vamos criar uma regra onde o jogador não pode retornar ""
         */
        if(key == "37" && direcao != "right")
        {
            direcao = "left";
        }
        else if(key == "38" && direcao != "down")
        {
            direcao = "up";
        }
        else if(key == "39" && direcao != "left")
        {
            direcao = "right";
        }
        else if(key == "40" && direcao != "up")
        {
            direcao = "down";
        }
        /**
         * Nosso controle está pronto agora é possível controlar a cobra
         */
    })

    //Assim que a página estiver toalmente carrega iniciamos o jogo
    iniciarJogo();
});


//--------------------------------------------------------------------------------------
// Sistema do jogo
//--------------------------------------------------------------------------------------
/**
 * Função é responsável por iuniciar o logo
 * @method iniciarJogo
 */
function iniciarJogo()
{
    //Por padrão o a cobra vai para direita
    direcao = "right";

    //Vamos realizar o nascimento da cobra
    nascimentoCobra();

    //Vamos realizar o nascimento do rato
    nascimentoRato();

    //Os pontos serão zerados a cada inicio de jogo
    pontos = 0;

    //Quando o objeto jogo já existe limpamos o intervalo de tempo
    if(typeof jogo != "undefined") clearInterval(jogo);
    jogo = setInterval(desenharJogo, 120);
}

/**
 *
 */
function desenharJogo()
{
    contexto2D.fillStyle = "#FFFFFF";
    contexto2D.fillRect(0, 0, largura, altura);
    contexto2D.strokeStyle = "rgba(75, 25, 75, 1)";
    contexto2D.strokeRect(0, 0, largura, altura);

    var nx = cobraCorpo[0].x;
    var ny = cobraCorpo[0].y;


    if(direcao == "right") nx++;
    else if(direcao == "left") nx--;
    else if(direcao == "up") ny--;
    else if(direcao == "down") ny++;

    if(nx == -1 || nx == largura/tamanhoCorpoCobra || ny == -1 || ny == altura/tamanhoCorpoCobra || verificarCerca(nx, ny, tamanhoCorpoCobra))
    {
        mensagemConsole("Fim de Jogo!");
        iniciarJogo();
        return;
    }

    if(nx == ratoPosicao.x && ny == ratoPosicao.y)
    {
        mensagemConsole("O rato foi pego, na posição "+nx+" | "+ny+"!");
        var tail = {x: nx, y: ny};
        pontos++;
        nascimentoRato();
    }
    else
    {
        var tail = cobraCorpo.pop();
        tail.x = nx; tail.y = ny;
    }
    cobraCorpo.unshift(tail);

    for(var i = 0; i < cobraCorpo.length; i++)
    {
        var c = cobraCorpo[i];
        corporCobra(c.x, c.y, i, cobraCorpo.length, direcao);
    }
    corpoRato(ratoPosicao.x, ratoPosicao.y);
    var pontos_text = "Pontos: " + pontos;
    contexto2D.fillStyle = "#000000";
    contexto2D.font="20px Georgia";
    contexto2D.fillText(pontos_text, 5, altura-5);
}

/**
 * Adiciona mensagem no console para exibição
 *
 * @param messagem
 */
function mensagemConsole(messagem)
{
    $("div.console").append(messagem + '<br>');
}

/**
 *
 * @param x
 * @param y
 * @param array
 * @returns {boolean}
 */
function verificarCerca(x, y, array)
{
    /**
     * Esta função irá verificar se existem as fornecidas coordenadas X / Y
     * em uma matriz de células ou não
     */
    for(var i = 0; i < array.length; i++)
    {
        if(array[i].x == x && array[i].y == y)
            return true;
    }
    return false;
}

//--------------------------------------------------------------------------------------
// Cobra
//--------------------------------------------------------------------------------------

/**
 * Criar a cobra na primeira posição do campo de jogo
 * com tamnho inicial já fixado.
 *
 * @method nascimentoCobra
 */
function nascimentoCobra()
{
    var cobraTamanho      = 5;  //Tamanho inicial da cobra
        cobraCorpo        = []; //Criamos um vetor vazio para preenchimento

    for(var i = cobraTamanho-1; i>=0; i--)
    {
        /**
         *  Isto irá criar uma cobra horizontal a partir do canto superior esquerdo
         *
         *  X = Coluna Horizontal do campo de jogo
         *  Y = Coluna Vertical   do campo de jogo
         */
        cobraCorpo.push({x: i, y:0});
    }
}
/**
 * Função irá colocar uma imagem como corpo do rato
 *
 * @param x
 * @param y
 * @param i
 * @param total
 * @param direcao
 */
function corporCobra(x, y, i, total, direcao)
{
    if(i == 0)
    {
        var img = new Image();

        img.src = "imagens/5_"+direcao+".png";
        img.onload = function () {
            var pattern = contexto2D.createPattern(img, "repeat");
            contexto2D.fillStyle = pattern;
            contexto2D.fillRect(x*tamanhoCorpoCobra, y*tamanhoCorpoCobra, tamanhoCorpoCobra, tamanhoCorpoCobra);
        };
    }
    else
    {
        var img = new Image();

        img.src = "imagens/1_"+direcao+".png";
        img.onload = function () {
            var pattern = contexto2D.createPattern(img, "repeat");
            contexto2D.fillStyle = pattern;
            contexto2D.fillRect(x*tamanhoCorpoCobra, y*tamanhoCorpoCobra, tamanhoCorpoCobra, tamanhoCorpoCobra);
        };
    }
}

//--------------------------------------------------------------------------------------
// Rato
//--------------------------------------------------------------------------------------

/**
 * Criar o rato em uma posição randômica no campo de jogo.
 *
 * @method nascimentoRato
 */
function nascimentoRato()
{
    /**
     * Isto irá criar uma célula com x / y entre 0-29
     * Porque existem 30 (900/30) posições atravessando as linhas e colunas
     */
    ratoPosicao = {
        x: Math.round(Math.random()*(largura-tamanhoCorpoCobra)/tamanhoCorpoCobra),
        y: Math.round(Math.random()*(altura-tamanhoCorpoCobra)/tamanhoCorpoCobra)
    };
}

/**
 * Função irá colocar uma imagem como corpo do rato
 *
 * @param int x Posição que o rato foi criado em na função nascimentoRato()
 * @param int y Posição que o rato foi criado em na função nascimentoRato()
 *
 * @method corpoRato
 */
function corpoRato(x, y)
{
    var img = new Image();
    img.src = "imagens/desenho-rato.png";
    img.onload = function ()
    {
        var pattern = contexto2D.createPattern(img, "repeat");
        contexto2D.fillStyle = pattern;
        contexto2D.fillRect(x*tamanhoCorpoRato, y*tamanhoCorpoRato, tamanhoCorpoRato, tamanhoCorpoRato);
    };
}
