<!DOCTYPE HTML>
<html>
<head>
    <!-- Setar metatags obrigatórias-->
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Jogo Cobra - Versão 1.0</title>

    <!-- Carregar estilo do jogo-->
    <link href="css/cobra.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>

<div id="corpo">
    <div class="logo">
        <img src="../imagens/logo1.png" alt="" width="250px">
    </div>

    <div id="container">
        <canvas id="campoDeJogo" width="750" height="600"></canvas>
        <div class="console"></div>
    </div>
</div>

<!--Carregar dependências-->
<script src="../js/jquery.min.js" type="text/javascript"></script>
<!--Carregar jogo-->
<script src="js/cobra.js" type="text/javascript"></script>

</body>
</html>