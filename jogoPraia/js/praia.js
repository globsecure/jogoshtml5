

// Elemento ready é criado para executar as funções
// apenas depois que a página está carregada
$(document).ready(function(){

    $("#palavra").on("change", function(e, animal){
        loadImages(sources, $(this).val(), initStage);
    });


});
function loadImages(sources, palavra, callback)
{
    var assetDir = 'imagens/';
    var images = {};
    var loadedImages = 0;
    var numImages = 0;
    for(var src in sources) {
        numImages++;
    }
    for(var src in sources) {
        images[src] = new Image();
        images[src].onload = function() {
            if(++loadedImages >= numImages) {
                callback(images, palavra);
            }
        };
        images[src].src = assetDir + sources[src];
    }
}

function isNearOutline(animal, outline) {
    var a = animal;
    var o = outline;
    var ax = a.getX();
    var ay = a.getY();

    if(ax > o.x - 20 && ax < o.x + 20 && ay > o.y - 20 && ay < o.y + 20) {
        return true;
    }
    else {
        return false;
    }
}
function drawBackground(background, beachImg, text) {
    var context = background.getContext();

    context.drawImage(beachImg, 0, 0);
    context.setAttr('font', '20pt Calibri');
    context.setAttr('textAlign', 'center');
    context.setAttr('fillStyle', 'white');
    context.fillText(text, background.getStage().getWidth() / 2, 40);
}
function initStage(images, palavra) {
    var stage = new Kinetic.Stage({
        container: 'jogo',
        width: 578,
        height: 530
    });

    var background = new Kinetic.Layer();
    var animalLayer = new Kinetic.Layer();
    var animalShapes = [];
    var score = 0;

    // image positions
    var animals = {
        snake: {
            x: 10,
            y: 70
        },
        giraffe: {
            x: 90,
            y: 70
        },
        monkey: {
            x: 275,
            y: 70
        },
        lion: {
            x: 400,
            y: 70
        },
    };

    var outlines = {
        snake_black: {
            x: 275,
            y: 350
        },
        giraffe_black: {
            x: 390,
            y: 250
        },
        monkey_black: {
            x: 300,
            y: 420
        },
        lion_black: {
            x: 100,
            y: 390
        },
    };

    // create draggable animals
    for(var key in animals) {
        // anonymous function to induce scope
        (function() {
            var privKey = key;
            var anim = animals[key];

            var animal = new Kinetic.Image({
                image: images[key],
                x: anim.x,
                y: anim.y,
                draggable: true,
                brightness: 0,
                blurRadius: 0
            });

            animal.cache();
            animal.drawHitFromCache();
            animal.filters([
                Kinetic.Filters.Blur,
                Kinetic.Filters.Brighten
            ]);

            animal.on('dragstart', function() {
                this.moveToTop();
                animalLayer.draw();
            });
            /*
             * check if animal is in the right spot and
             * snap into place if it is
             */
            animal.on('dragend', function() {
                var outline = outlines[privKey + '_black'];
                if(!animal.inRightPlace && isNearOutline(animal, outline)) {
                    animal.setPosition({x:outline.x, y:outline.y});
                    animalLayer.draw();
                    animal.inRightPlace = true;

                    if(++score >= 4) {
                        var text = 'Você ganhou! Parabéns'
                        drawBackground(background, images.beach, text);
                    }

                    // disable drag and drop
                    setTimeout(function() {
                        animal.setDraggable(false);
                    }, 50);
                }
            });

            console.log(palavra);

            var amplitude = 150;
            var period = 2000;
            // in ms
            var centerX = stage.width()/2;


            if(palavra == 'Cobra' && key == 'snake')
            {
                animal.blurRadius(1);
                animal.brightness(0.3);


                var anim = new Kinetic.Animation(function(frame) {
                    animal.setX(amplitude * Math.sin(frame.time * 2 * Math.PI / period) + centerX);
                }, animalLayer);
                anim.start();
            }
            if(palavra == 'Girafa' && key == 'giraffe')
            {
                animal.blurRadius(1);
                animal.brightness(0.3);


                var anim = new Kinetic.Animation(function(frame) {
                    animal.setX(amplitude * Math.sin(frame.time * 2 * Math.PI / period) + centerX);
                }, animalLayer);
                anim.start();
            }
            if(palavra == 'Macaco' && key == 'monkey')
            {
                animal.blurRadius(1);
                animal.brightness(0.3);


                var anim = new Kinetic.Animation(function(frame) {
                    animal.setX(amplitude * Math.sin(frame.time * 2 * Math.PI / period) + centerX);
                }, animalLayer);
                anim.start();
            }
            if(palavra == 'Leão' && key == 'lion')
            {
                animal.blurRadius(1);
                animal.brightness(0.3);


                var anim = new Kinetic.Animation(function(frame) {
                    animal.setX(amplitude * Math.sin(frame.time * 2 * Math.PI / period) + centerX);
                }, animalLayer);
                anim.start();
            }

            // make animal glow on mouseover
            animal.on('mouseover touchstart', function() {
                animal.blurRadius(1);
                animal.brightness(0.3);
                animalLayer.draw();
                document.body.style.cursor = 'pointer';
            });
            // return animal on mouseout
            animal.on('mouseout touchend', function() {
                animal.blurRadius(0);
                animal.brightness(0);
                animalLayer.draw();
                document.body.style.cursor = 'default';
            });

            animal.on('dragmove', function() {
                document.body.style.cursor = 'pointer';
            });

            window.addEventListener('change', function(e, animal) {
                return animal;
            });

            animalLayer.add(animal);
            animalShapes.push(animal);
        })();
    }

    // create animal outlines
    for(var key in outlines) {
        // anonymous function to induce scope
        (function() {
            var imageObj = images[key];
            var out = outlines[key];

            var outline = new Kinetic.Image({
                image: imageObj,
                x: out.x,
                y: out.y
            });

            animalLayer.add(outline);
        })();
    }

    stage.add(background);
    stage.add(animalLayer);

    drawBackground(background, images.beach, 'Coloque os animais na praia!');
}


var sources = {
    beach: 'beach.png',
    snake: 'snake.png',
    snake_black: 'snake-black.png',
    lion: 'lion.png',
    lion_black: 'lion-black.png',
    monkey: 'monkey.png',
    monkey_black: 'monkey-black.png',
    giraffe: 'giraffe.png',
    giraffe_black: 'giraffe-black.png'
};

loadImages(sources, palavra, initStage);