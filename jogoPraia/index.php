<!DOCTYPE HTML>
<html>
<head>
    <!-- Setar metatags obrigatórias-->
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Jogo Cobra - Versão 1.0</title>

    <!-- Carregar estilo do jogo-->
    <link href="css/praia.css" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../css/speech-input.css">
</head>
<body>

<div id="corpo">


    <div class="logo">
        <img src="../imagens/logo1.png" alt="" width="250px">
    </div>

    <div id="container">
        <div id="jogo"></div>
        <div class="speech-input-wrapper"><input id="palavra" value="Fale o nome do animal" type="text"></div>
    </div>

</div>

    <!--Carregar dependências-->
    <script src="../js/kinetic-v5.0.1.min.js"></script>
    <script src="../js/jquery.min.js" type="text/javascript"></script>
    <script src="../js/speech-input.js" type="text/javascript"></script>

    <!--Carregar jogo-->
    <script src="js/praia.js" type="text/javascript"></script>
</body>
</html>